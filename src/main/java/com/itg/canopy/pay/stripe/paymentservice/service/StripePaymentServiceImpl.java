package com.itg.canopy.pay.stripe.paymentservice.service;


import com.itg.canopy.pay.stripe.paymentservice.dto.Request;
import com.itg.canopy.pay.stripe.paymentservice.dto.Response;
import com.itg.canopy.pay.stripe.paymentservice.service.PaymentService;
import com.stripe.Stripe;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StripePaymentServiceImpl implements PaymentService {

    @Value("${stripe.key}")
    private String key;


    @Override
    public Response authenticatePayment(Request request){

        Stripe.apiKey = key;
        PaymentIntent intent = new PaymentIntent();


        long value = Double.valueOf(request.getValue() * 100).longValue();
        System.out.println("VALUE-----" + value);

        try {

            if (request.getPaymentId() != null) {
                PaymentIntentCreateParams createParams = PaymentIntentCreateParams.builder()
                        .setAmount(value)
                        .setCurrency("gbp")
                        .setConfirm(true)
                        .setPaymentMethod(request.getPaymentId())
                        .setConfirmationMethod(PaymentIntentCreateParams.ConfirmationMethod.MANUAL)
                        .putExtraParam("setup_future_usage", "on_session")
                        .build();
                intent = PaymentIntent.create(createParams);
            } else if (request.getPaymentIntentId() != null) {
                intent = PaymentIntent.retrieve(request.getPaymentIntentId());
                intent = intent.confirm();
            }
            Response response = generatePaymentResponse(intent);
            System.out.println(intent.getId());
            return response;

        }catch (Exception e){
            Response response = new Response();
            response.setError(e.toString());
            return response;
        }

    }


    private Response generatePaymentResponse(PaymentIntent intent){

        Response response = new Response();

        if (intent.getStatus().equals("requires_action")
                && intent.getNextAction().getType().equals("use_stripe_sdk")) {
            response.setRequires_action(true);
            response.setPayment_intent_client_secret(intent.getClientSecret());
        } else if (intent.getStatus().equals("succeeded")) {
            response.setSuccess(true);
        } else {
            // invalid status
            response.setError("Invalid status");
            return response;
        }
        return response;
    }


}
