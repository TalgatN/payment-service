package com.itg.canopy.pay.stripe.paymentservice.controller;

import com.itg.canopy.pay.stripe.paymentservice.dto.Request;
import com.itg.canopy.pay.stripe.paymentservice.dto.Response;
import com.itg.canopy.pay.stripe.paymentservice.service.StripePaymentServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin("*")
public class PaymentController {

    @Autowired
    StripePaymentServiceImpl stripePaymentServiceImpl;

    @ApiOperation(value = "/confirm",authorizations = {@Authorization(value = "auth0_jwt")})
    @PostMapping(value = "/confirm", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> confirmPayment(@RequestBody Request request){

        Response response = stripePaymentServiceImpl.authenticatePayment(request);

        ResponseEntity<Response> stripeResponse;

        if(response.getError() == null) {
            stripeResponse = new ResponseEntity(response, HttpStatus.OK);
        }else{
            stripeResponse = new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return stripeResponse;
    }


}
