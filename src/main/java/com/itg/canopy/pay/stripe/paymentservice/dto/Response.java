package com.itg.canopy.pay.stripe.paymentservice.dto;

public class Response {

    private boolean requires_action;
    private String payment_intent_client_secret;
    private boolean success;
    private String Error;


    public Response() {
        //default constructor
    }

    public boolean isRequires_action() {
        return requires_action;
    }

    public void setRequires_action(boolean requires_action) {
        this.requires_action = requires_action;
    }

    public String getPayment_intent_client_secret() {
        return payment_intent_client_secret;
    }

    public void setPayment_intent_client_secret(String payment_intent_client_secret) {
        this.payment_intent_client_secret = payment_intent_client_secret;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }

    @Override
    public String toString() {
        return "Response{" +
                "requires_action='" + requires_action + '\'' +
                ", payment_intent_client_secret='" + payment_intent_client_secret + '\'' +
                ", success=" + success +
                ", Error='" + Error + '\'' +
                '}';
    }
}
