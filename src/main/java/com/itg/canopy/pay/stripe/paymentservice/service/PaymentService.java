package com.itg.canopy.pay.stripe.paymentservice.service;

import com.itg.canopy.pay.stripe.paymentservice.dto.Request;
import com.itg.canopy.pay.stripe.paymentservice.dto.Response;

public interface PaymentService {

    public Response authenticatePayment(Request request);
}
