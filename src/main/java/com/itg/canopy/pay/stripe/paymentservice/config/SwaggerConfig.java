package com.itg.canopy.pay.stripe.paymentservice.config;

import io.swagger.models.Swagger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private SecurityScheme securityScheme(){
        List<GrantType> grantTypes = new ArrayList<>();
        ImplicitGrant implicitGrant = new ImplicitGrant(new LoginEndpoint("http://localhost:8080/oauth/authorize"),"access_code");
        grantTypes.add(implicitGrant);
        SecurityScheme oauth = new OAuthBuilder().name("auth0_jwt").grantTypes(grantTypes).build();
        return oauth;
    }


    @Bean
    public Docket paymentService(){

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.itg.canopy.pay.stripe.paymentservice.controller"))
                .paths(regex("/.*"))
                .build()
                .host("http://localhost:8080")
                .apiInfo(metaData()).securitySchemes(Arrays.asList(securityScheme()));

    }

    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "Payment Service API",
                "Payment Service API",
                "1.0",
                "Terms of service",
                new Contact("Kashif Abro", "https://springframework.guru/about/", "test.co.uk"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0");
        return apiInfo;
    }


}
