package com.itg.canopy.pay.stripe.paymentservice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Request {

    @ApiModelProperty(notes = "Token ID generated by Stripe")
    private String paymentId;
    @ApiModelProperty(notes = "Payment Intent ID for retrying a charge")
    private String paymentIntentId;
    @ApiModelProperty(notes = "Value wished to charge customer")
    private double value;

    public Request() {
        //default constructor
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentIntentId() {
        return paymentIntentId;
    }

    public void setPaymentIntentId(String paymentIntentId) {
        this.paymentIntentId = paymentIntentId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Request{" +
                "paymentId='" + paymentId + '\'' +
                ", paymentIntentId='" + paymentIntentId + '\'' +
                ", value=" + value +
                '}';
    }
}
