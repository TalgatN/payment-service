FROM openjdk:8-alpine
ADD target/payment-service.jar payment-service.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "payment-service.jar"]